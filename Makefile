SRC_DIR = ./Source

DEBUG = 0
CXX = clang++
TARGET_NAME = rhdump

SRCS = main.cpp Dumper.cpp

DEFAULT_FLAGS = -std=c++17 -Wno-c++98-compat -Wall -Wextra -Wno-unused-parameter -Wno-unused-private-field
OPT_FLAGS = -Ofast -march=bdver1 -mtune=bdver1
DBG_FLAGS = -O0 -g

LD = $(CXX)
CXXFLAGS = $(DEFAULT_FLAGS)

ifeq ($(DEBUG), 1)
	CXXFLAGS += $(DBG_FLAGS)
	LDFLAGS = -fuse-ld=lld -lstdc++fs
	BIN_DIR = ./Binary/Debug/
	OBJ_DIR = ./Binary/Debug/obj
else
	CXXFLAGS += $(OPT_FLAGS)
	LDFLAGS = -fuse-ld=lld -flto -O3 -march=native -mtune=native -s -lstdc++fs
	BIN_DIR = ./Binary/Release/
	OBJ_DIR = ./Binary/Release/obj
endif

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cpp
	$(CXX) $^ -o $@ -c $(CXXFLAGS)

TARGET_BIN = $(BIN_DIR)/$(TARGET_NAME)

all : make_dirs $(TARGET_BIN)

$(TARGET_BIN) : $(SRCS:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) -o $@ $^ $(LDFLAGS)

clean:
	rm -rf ./Binary/Release/*
	rm -rf ./Binary/Debug/*

make_dirs :
	mkdir -p $(OBJ_DIR)
	mkdir -p $(BIN_DIR)
