#include "Dumper.hpp"

std::string CROMDumper::ReadString(std::fstream& File, __uint32_t ReadSize, bool Region)
{
    char TempBuffer[g_BufferSize] = {0};

    File.read(TempBuffer, ReadSize);

    std::string TempString = {TempBuffer};
    TempString = TempString.substr(0, TempString.find_last_not_of(" ") + 1);

    if(Region)
        TempString.erase(std::remove_if(TempString.begin(), TempString.end(), ::isspace), TempString.end());

    return TempString;
}

template<typename __Mty> std::string CROMDumper::ReadInt(std::fstream& File, bool Seek, __uint32_t SeekPos)
{
    __Mty Buffer = 0;

    if(Seek)
    {
        File.seekg(SeekPos);
    }
    File.read(reinterpret_cast<char*>(&Buffer), sizeof(__Mty));
    return fmt::format("0x{:X}", Buffer);
}

bool CROMDumper::ReadRom(const fs::path Path)
{
    const std::string PathString = Path.string();
    m_szROMFileName = PathString.substr(PathString.find_last_of("/") + 1);


    m_fsROMFile.open(Path, std::ios::in | std::ios::binary);
    if(m_fsROMFile)
    {
        m_fsROMFile.seekg(g_HeaderStart);
        m_rhROMHeader.ConsoleName    = ReadString(m_fsROMFile, 0x10);//Console Name
        m_rhROMHeader.ReleaseDate    = ReadString(m_fsROMFile, 0x10);//Release Date
        m_rhROMHeader.DomesticName   = ReadString(m_fsROMFile, 0x30);//Domestic Name
        m_rhROMHeader.OverseasName   = ReadString(m_fsROMFile, 0x30);//International Name
        m_rhROMHeader.SerialNumber   = ReadString(m_fsROMFile, 0xE);//Serial Number
        m_rhROMHeader.Checksum       = ReadInt<__uint16_t>(m_fsROMFile);//Checksum
        m_rhROMHeader.IOSupport      = ReadString(m_fsROMFile, 0x10);// I/O Support
        m_rhROMHeader.StartROM       = ReadInt<__uint32_t>(m_fsROMFile);//RStart
        m_rhROMHeader.EndROM         = ReadInt<__uint32_t>(m_fsROMFile);//REnd
        m_rhROMHeader.StartBackupRAM = ReadInt<__uint32_t>(m_fsROMFile);//BStart
        m_rhROMHeader.EndBackupRAM   = ReadInt<__uint32_t>(m_fsROMFile);//BEnd
        m_rhROMHeader.ModemSupport   = ReadString(m_fsROMFile, 0x10);//Modem Support
        m_rhROMHeader.Notes          = ReadString(m_fsROMFile, 0x28);//Notes
        m_rhROMHeader.RegionInfo     = ReadString(m_fsROMFile, 0x10, true);//Region
        
        m_fsROMFile.close();
        return true;
    }
    else
        return false;
    
    return false;
}

void CROMDumper::PrintHeader(bool RegionOnly)
{
    if(RegionOnly)
    {
        fmt::print("Region: {}\n", m_rhROMHeader.RegionInfo);
    }
    else
    {
        fmt::print("Console Name:     [{}]\n", m_rhROMHeader.ConsoleName);
        fmt::print("Release Date:     [{}]\n", m_rhROMHeader.ReleaseDate);
        fmt::print("Domestic Name:    [{}]\n", m_rhROMHeader.DomesticName);
        fmt::print("Overseas Name:    [{}]\n", m_rhROMHeader.OverseasName);
        fmt::print("Serial Number:    [{}]\n", m_rhROMHeader.SerialNumber);
        fmt::print("Checksum:         [{}]\n", m_rhROMHeader.Checksum );
        fmt::print("I/O Support:      [{}]\n", m_rhROMHeader.IOSupport);
        fmt::print("Rom Start:        [{}]\n", m_rhROMHeader.StartROM);
        fmt::print("Rom End:          [{}]\n", m_rhROMHeader.EndROM);
        fmt::print("BackupRAM Start:  [{}]\n", m_rhROMHeader.StartBackupRAM);
        fmt::print("BackupRAM End:    [{}]\n", m_rhROMHeader.EndBackupRAM);
        fmt::print("Modem Support:    [{}]\n", m_rhROMHeader.ModemSupport);
        fmt::print("Notes:            [{}]\n", m_rhROMHeader.Notes );
        fmt::print("Region:           [{}]\n", m_rhROMHeader.RegionInfo );
    }
}
