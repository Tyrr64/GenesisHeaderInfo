#include "Includes.hpp"

class CROMDumper
{
private:
    std::fstream m_fsROMFile;

    //Directory Mode
    fs::path m_phDirectoryPath = "";
    std::vector<std::pair<std::string, GHeader_t>> m_vROMList;

    //Single Mode
    std::string m_szROMFileName = "";
    GHeader_t m_rhROMHeader;

public:

    std::string ReadString(std::fstream& ROMFile, __uint32_t ReadSize, bool Region = false);
    template<typename __Mty> std::string ReadInt(std::fstream& File, bool Seek = false, __uint32_t SeekPos = 0);

    bool ReadRom(const fs::path FilePath);

    bool ReadDirectory(const fs::path DirPath);

    void PrintHeader(bool RegionOnly = false);
};