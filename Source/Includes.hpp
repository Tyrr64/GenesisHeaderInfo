#include <string>
#include <vector>
#include <fstream>
#include <filesystem>

#define FMT_HEADER_ONLY
#include "fmt/core.h"

namespace fs = std::filesystem;

inline constexpr const uint16_t g_HeaderStart = 0x0100;
inline constexpr const uint32_t g_BufferSize = 64;

typedef struct __GENSIS_ROM_HEADER__
{
    std::string  ConsoleName     = "";
    std::string  ReleaseDate     = "";
    std::string  DomesticName    = "";
    std::string  OverseasName    = "";
    std::string  SerialNumber    = "";
    std::string  Checksum        = "";
    std::string  IOSupport       = "";
    std::string  StartROM        = "";
    std::string  EndROM          = "";
    std::string  StartBackupRAM  = "";
    std::string  EndBackupRAM    = "";
    std::string  ModemSupport    = "";
    std::string  Notes           = "";
    std::string  RegionInfo      = "";
} GHeader_t;