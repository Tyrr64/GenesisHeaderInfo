#include "Dumper.hpp"

int main(int argc, char** argv)
{
    CROMDumper dumper;

    if(argc == 2)
    {
        dumper.ReadRom(argv[1]);
        dumper.PrintHeader();
    }
    else if(argc == 3)
    {
        dumper.ReadRom(argv[1]);
        dumper.PrintHeader(true);
    }
    else
    {
        fmt::print("Usage: rhdump [path to rom] [*optinal* 'r' dumps region only]\n");
    }

    return 0;
}